module.exports = (ctx) => ({
  plugins: [
    require('postcss-import')(),
    require('postcss-mixins')(),
    require('postcss-each')(),
    require('postcss-for')(),
    require('postcss-modules-values'),
    require('postcss-flexbugs-fixes'),
    require('postcss-cssnext')({
      browsers: [
        '>1%',
        'last 4 versions',
        'Firefox ESR',
        'not ie < 9', // React doesn't support IE8 anyway
      ],
      flexbox: 'no-2009',
    }),
    require('cssnano')({
      autoprefixer: false, // cssnext does this
      normalizeUrl: false, // url loader does this
      zindex: false, // not safe cross chunks
      reduceIdents: false, // not safe cross chunks
      discardUnused: false, // not safe as could be referenced by css module template / cross chunk
      discardEmpty: false // not safe as could be referenced by css module template / cross chunk
    }),
    require('postcss-reporter')({ clearMessages: true })
  ]
})
