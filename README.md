## Overview

- Written in `typescript` transpiled to ES6.
- Side-effect orchestration using `redux-saga`
- Styling via `CSS Modules` & `PostCSS`
- Basic responsiveness via flex layout
- i18n & l10n support via `react-intl`
- Unit tests throughout

### Dependencies

```npm install -g yarn && yarn```

The tests have issues with watching out the box when precompiling with typescript, and will likely bomb out.
To avoid this and run the tests successfully please install Watchman which can handle larger workloads...

```brew install watchman```

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.

### `yarn build`

Builds the production artifacts.

## Fututure improvements

- Stub console error calls in unit tests to clean up the output a bit
- Inlining of webpack manifest and runtime, and curate chunks
- Local express server to run and validate built production artifacts
- More error handling in CSV parsing (ie when empty, or cant parse date etc)
- Better layout for when there is only a partial first day
- Move cities to config rather than hard coded in component (or look up from API)
- Resolve locale from browser settings, use for i18n & l10n (supports i18n & l10n fully, but its just set to a default `en` at moment - which is likely American hence the date format being incorrect - and no translation messages etc yet)
- Test containers using mock store in React context
- Someone else's design skillz
- More Nic Cage references
