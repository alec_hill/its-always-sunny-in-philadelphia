import {createAction} from 'redux-actions'
import prefix from '../__util__/prefix'

const name = prefix('location')

export const CHANGE_CITY = name('city', 'change')
export const CSV = name('csv')

export const changeCity = createAction<string>(CHANGE_CITY)
export const submitCSV = createAction<string>(CSV)
