import {Selector} from 'reselect'
import {IAppState} from '../domain'

export const city: Selector<IAppState, string> = state =>  state.location.city
