import * as React from 'react'
import {FormattedMessage} from 'react-intl'

const styles = require('./CSVEntryForm.module.css')

export interface ICSVEntryFormStateProps {
  activeCity: string
}

export interface ICSVEntryFormDispatchProps {
  submitCSV: (csv: string) => void
}

export interface ICSVEntryFormProps extends ICSVEntryFormStateProps, ICSVEntryFormDispatchProps {}

export interface ICSVEntryFormState {
  csv: string
}

export class CSVEntryForm extends React.PureComponent<ICSVEntryFormProps, ICSVEntryFormState> {

  constructor(props: ICSVEntryFormProps) {
    super(props)
    this.state = {
      csv: ''
    }
  }

  private onChange = (e: React.FormEvent<HTMLTextAreaElement>): void => {
    this.setState({csv: e.currentTarget.value})
  }

  private onSubmit = (e: React.FormEvent<HTMLFormElement>): void => {
    e.preventDefault()
    this.props.submitCSV(this.state.csv)
  }

  private isActive = () => this.props.activeCity === 'Custom CSV'

  componentWillReceiveProps(props: ICSVEntryFormProps) {
    if (props.activeCity !== this.props.activeCity) this.setState({csv: ''})
  }

  render() {
    return this.isActive() ? (
      <form className={styles.form} onSubmit={this.onSubmit}>
        <textarea className={styles.csvText} onChange={this.onChange} value={this.state.csv} />
        <button type="submit" className={styles.submit}>
          <FormattedMessage id="csv.form.submit" defaultMessage="Submit" />
        </button>
      </form>
    ) : null
  }

}

export default CSVEntryForm
