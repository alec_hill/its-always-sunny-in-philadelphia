import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import {CSVEntryForm} from '../CSVEntryForm'

const styles = require('../CSVEntryForm.module.css')

describe('CSVEntryForm component', () => {

  const mockSubmitCSV = jest.fn()

  afterEach(mockSubmitCSV.mockReset)

  describe('when is Custom CSV is active', () => {

    const wrapper = enzymeIntl.shallow(<CSVEntryForm activeCity="Custom CSV" submitCSV={mockSubmitCSV} />)

    it('should have a textarea', () => {
      expect(wrapper.find('textarea')).toHaveLength(1)
    })

    it('should have a submit button', () => {
      expect(wrapper.find('button')).toHaveLength(1)
    })

    describe('when text is entered is submitted', () => {

      const stubCSV = 'nic, cage'
      wrapper.find('textarea').simulate('change', {currentTarget: {value: stubCSV}, preventDefault: jest.fn()})

      describe('and the form is submitted', () => {

        it('should call the submitCSV callback with CSV a CityButton for Custom CSV', () => {
          wrapper.find('form').simulate('submit', {preventDefault: jest.fn()})
          expect(mockSubmitCSV).toHaveBeenCalledWith(stubCSV)
        })

      })

    })

  })

  describe('when is Custom CSV is not active', () => {

    const wrapper = enzymeIntl.shallow(<CSVEntryForm activeCity="nic cage" submitCSV={mockSubmitCSV} />)

    it('should not render anything', () => {
      expect(wrapper.children()).toHaveLength(0)
    })

  })

})
