import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import {CitySelector} from '../CitySelector'
import CityButtonContainer from '../../../containers/CityButtonContainer'
import CSVEntryFormContainer from '../../../containers/CSVEntryFormContainer'

const styles = require('../CitySelector.module.css')

describe('CitySelector component', () => {

  it('should contain multiple CityButton components', () => {
    const wrapper = enzymeIntl.shallow(<CitySelector />)
    expect(wrapper.find(CityButtonContainer).length).toBeGreaterThan(1)
  })

  it('should contain a CityButton for Custom CSV', () => {
    const wrapper = enzymeIntl.shallow(<CitySelector />)
    expect(wrapper.find(CityButtonContainer).last().props().city).toBe('Custom CSV')
  })

})
