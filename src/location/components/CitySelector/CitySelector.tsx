import * as React from 'react'
import CityButtonContainer from '../../containers/CityButtonContainer'
import CSVEntryFormContainer from '../../containers/CSVEntryFormContainer'

const styles = require('./CitySelector.module.css')

export class CitySelector extends React.PureComponent {

  render() {
    return (
      <>
        <nav className={styles.buttons}>
          <CityButtonContainer city="London" />
          <CityButtonContainer city="York" />
          <CityButtonContainer city="Cardiff" />
          <CityButtonContainer city="Edinburgh" />
          <CityButtonContainer city="Inverness" />
          <CityButtonContainer city="Custom CSV" />
        </nav>
        <CSVEntryFormContainer />
      </>
    )
  }

}

export default CitySelector
