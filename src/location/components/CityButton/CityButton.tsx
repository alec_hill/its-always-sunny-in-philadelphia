import * as React from 'react'
import * as classnames from 'classnames'
import {FormattedMessage} from 'react-intl'

const styles = require('./CityButton.module.css')

export interface ICityButtonOwnProps {
  city: string
}

export interface ICityButtonStateProps {
  activeCity: string
}

export interface ICityButtonDispatchProps {
  changeCity: (city: string) => void
}

export interface ICityButtonProps extends ICityButtonOwnProps, ICityButtonStateProps, ICityButtonDispatchProps {}

export class CityButton extends React.PureComponent<ICityButtonProps> {

  private isSelected = () => this.props.city === this.props.activeCity

  private onClick = () => this.props.changeCity(this.props.city)

  render() {
    const {city} = this.props
    return (
      <button onClick={this.onClick}
              className={classnames(styles.button, {
                          [styles.selected]: !!this.isSelected()
                        })}>
        <FormattedMessage id={`location.city.button.${city}`} defaultMessage={city} />
      </button>
    )
  }

}

export default CityButton
