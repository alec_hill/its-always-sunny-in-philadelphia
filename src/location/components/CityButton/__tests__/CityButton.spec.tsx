import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import {FormattedMessage} from 'react-intl'
import {CityButton} from '../CityButton'

const styles = require('../CitySelector.module.css')

describe('CityButton component', () => {

  const activeCity = 'nic'
  const city = 'cage'
  const mockChangeCity = jest.fn()

  afterEach(mockChangeCity.mockReset)

  it('should contain formatted city name', () => {
    const wrapper = enzymeIntl.shallow(<CityButton city={city} activeCity={activeCity} changeCity={mockChangeCity} />)
    expect(wrapper.find(FormattedMessage).props().defaultMessage).toBe(city)
  })

  describe('when is the active city', () => {

    const wrapper = enzymeIntl.shallow(<CityButton city={city} activeCity={city} changeCity={mockChangeCity} />)

    it('should have selected button styles', () => {
      expect(wrapper.find('button').hasClass(styles.selected)).toBe(true)
    })

  })

  describe('when is not the active city', () => {

    const wrapper = enzymeIntl.shallow(<CityButton city={city} activeCity={activeCity} changeCity={mockChangeCity} />)

    it('should not have selected button styles', () => {
      expect(wrapper.find('button').hasClass(styles.selected)).toBe(false)
    })

  })

  describe('when button is clicked', () => {

    it('should call the changeCity callback with the city value', () => {
      const wrapper = enzymeIntl.shallow(<CityButton city={city} activeCity={activeCity} changeCity={mockChangeCity} />)
      wrapper.find('button').simulate('click')
      expect(mockChangeCity).toHaveBeenCalledWith(city)
    })

  })

})
