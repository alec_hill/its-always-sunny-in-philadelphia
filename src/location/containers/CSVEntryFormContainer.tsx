import {
  connect,
  MapStateToProps,
  MapDispatchToProps
} from 'react-redux'
import {
  bindActionCreators,
  Dispatch
} from 'redux'
import {IAppState} from '../../domain'
import * as selectors from '../selectors'
import * as actions from '../actions'
import {
  ICSVEntryFormDispatchProps,
  ICSVEntryFormStateProps,
  CSVEntryForm
} from '../components/CSVEntryForm/CSVEntryForm'

const mapStateToProps: MapStateToProps<ICSVEntryFormStateProps, {}, IAppState> = state => ({
  activeCity: selectors.city(state)
})

const mapDispatchToProps: MapDispatchToProps<ICSVEntryFormDispatchProps, {}> = (dispatch: Dispatch<IAppState>) => ({
  submitCSV: (csv: string) => dispatch(actions.submitCSV(csv))
})

export default connect<ICSVEntryFormStateProps, ICSVEntryFormDispatchProps>
  (mapStateToProps, mapDispatchToProps)(CSVEntryForm)
