import {
  connect,
  MapStateToProps,
  MapDispatchToProps
} from 'react-redux'
import {
  bindActionCreators,
  Dispatch
} from 'redux'
import {IAppState} from '../../domain'
import * as selectors from '../selectors'
import * as actions from '../actions'
import {
  ICityButtonDispatchProps,
  ICityButtonOwnProps,
  ICityButtonProps,
  ICityButtonStateProps,
  CityButton
} from '../components/CityButton/CityButton'

const mapStateToProps: MapStateToProps<ICityButtonStateProps, ICityButtonOwnProps, IAppState> = state => ({
  activeCity: selectors.city(state)
})

const mapDispatchToProps: MapDispatchToProps<ICityButtonDispatchProps, ICityButtonOwnProps> =
  (dispatch: Dispatch<IAppState>) => ({
    changeCity: (city: string) => dispatch(actions.changeCity(city))
  })

export default connect<ICityButtonStateProps, ICityButtonDispatchProps, ICityButtonOwnProps>
  (mapStateToProps, mapDispatchToProps)(CityButton)
