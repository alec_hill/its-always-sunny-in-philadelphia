import * as actions from '../actions'
import * as reducers from '../reducers'
import {INITIAL_STATE} from '../../state'

describe('Location Reducers', () => {

  describe('city reducer', () => {

    it('should defaut if previous state is not specified', () => {
      const action = { type: 'nic cage' }
      expect(reducers.city(undefined, action)).toEqual(INITIAL_STATE.location.city)
    })

    it('should return the current state if action is given but not matched', () => {
      const state = 'nic'
      expect(reducers.city(state, { type: 'nic cage' })).toEqual(state)
    })

    it('should return a new state with changed city', () => {
      const state = 'nic'
      const payload = 'cage'
      const nextState = reducers.city(state, { type: actions.CHANGE_CITY, payload })

      expect(state).toBe('nic')
      expect(nextState).toBe(payload)
    })

  })

})
