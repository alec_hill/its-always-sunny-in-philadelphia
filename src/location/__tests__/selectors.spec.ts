import {createAppState} from '../../state'
import * as selectors from '../selectors'

describe('Location selectors', () => {

  const city = 'nic'
  const country = 'cage'

  describe('city', () => {

    it('should get the city from the state', () => {
      const state = Object.assign({}, createAppState(), {location: {city, country}})
      expect(selectors.city(state)).toEqual(city)
    })

  })

})
