import * as actions from '../actions'

describe('Location Actions', () => {

  describe('change city', () => {

    it('should be an available action', () => {
      expect(actions.CHANGE_CITY).toEqual('location/city/change')
    })

    it('should create the appropriate action containing csv string payload', () => {
      const stubCity = 'nic cage'
      const action = actions.changeCity(stubCity)

      expect(action.type).toEqual(actions.CHANGE_CITY)
      expect(action.payload).toEqual(stubCity)
    })

  })

  describe('submit csv', () => {

    it('should be an available action', () => {
      expect(actions.CSV).toEqual('location/csv')
    })

    it('should create the appropriate action containing csv string payload', () => {
      const stubCSV = 'nic,cage'
      const action = actions.submitCSV(stubCSV)

      expect(action.type).toEqual(actions.CSV)
      expect(action.payload).toEqual(stubCSV)
    })

  })

})
