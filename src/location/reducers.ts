import {handleActions} from 'redux-actions'
import * as actions from './actions'
import * as selectors from './selectors'
import { INITIAL_STATE } from '../state'

export const city = handleActions<string>({

  [actions.CHANGE_CITY]: (state, action) => action.payload

}, selectors.city(INITIAL_STATE))
