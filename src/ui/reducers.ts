import {handleActions} from 'redux-actions'
import * as actions from './actions'
import * as selectors from './selectors'
import { INITIAL_STATE } from '../state'

export const loading = handleActions<number>({

  [actions.LOADING_STARTED]: (state, action) => state + 1,

  [actions.LOADING_DONE]: (state, action) => state - 1

}, selectors.loadingCount(INITIAL_STATE))
