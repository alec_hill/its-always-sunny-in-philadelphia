import {createAction} from 'redux-actions'
import prefix from '../__util__/prefix'

const name = prefix('ui')

export const APPLICATION_INIT = name('application', 'init')
export const LOADING_STARTED = name('loading', 'started')
export const LOADING_DONE = name('loading', 'done')

export const applicationInit = createAction(APPLICATION_INIT)
export const loadingStarted = createAction(LOADING_STARTED)
export const loadingDone = createAction(LOADING_DONE)
