import {
  Selector,
  createSelector
} from 'reselect'
import {
  IAppState
} from '../domain'

export const loadingCount: Selector<IAppState, number> = state =>  state.ui.loading

export const isLoading = createSelector<IAppState, number, boolean>(loadingCount, num => num > 0)
