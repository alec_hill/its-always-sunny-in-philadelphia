import {createAppState} from '../../state'
import * as selectors from '../selectors'

describe('UI selectors', () => {

  describe('loading', () => {

    describe('count (number of different thiongs that have triggered a loading state)', () => {

      it('should get count of things that are loading from the state', () => {
        const stubCount = 666
        const state = createAppState()
        state.ui = Object.assign({}, state.ui , {loading: stubCount})
        expect(selectors.loadingCount(state)).toEqual(stubCount)
      })

    })

    describe('boolean flag (something is loading)', () => {

      it('should be false if there are zero things loading', () => {
        const state = createAppState()
        state.ui = Object.assign({}, state.ui , {loading: 0})
        expect(selectors.isLoading(state)).toBe(false)
      })

      it('should be true if there is a single thing loading', () => {
        const state = createAppState()
        state.ui = Object.assign({}, state.ui , {loading: 1})
        expect(selectors.isLoading(state)).toBe(true)
      })

      it('should be true if there is more than one things loading', () => {
        const state = createAppState()
        state.ui = Object.assign({}, state.ui , {loading: 2})
        expect(selectors.isLoading(state)).toBe(true)
      })

    })

  })

})
