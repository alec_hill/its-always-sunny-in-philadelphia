import * as actions from '../actions'
import * as reducers from '../reducers'
import {INITIAL_STATE} from '../../state'

describe('UI Reducers', () => {

  describe('loading reducer', () => {

    const loadingReducer = reducers.loading

    it('should defaut to 0 if previous state is not specified', () => {
      const action = { type: 'nic cage' }
      expect(loadingReducer(undefined, action)).toEqual(0)
    })

    it('should return the current state if action is given but not matched', () => {
      const state = 1
      expect(loadingReducer(state, { type: 'nic cage' })).toEqual(1)
    })

    describe('loading started ', () => {

      it('should return a new state signifying that one thing is loading', () => {
        const state = 0
        const nextState = loadingReducer(state, { type: actions.LOADING_STARTED })

        expect(nextState).toEqual(1)
      })

      it('should return a new state signifying that a second thing is loading', () => {
        const state = 1
        const nextState = loadingReducer(state, { type: actions.LOADING_STARTED })

        expect(nextState).toEqual(2)
      })

    })

    describe('loading done ', () => {

      it('should return a new state signifying that nothing is loading if one thing was previously loading', () => {
        const state = 1
        const nextState = loadingReducer(state, { type: actions.LOADING_DONE })

        expect(nextState).toEqual(0)
      })

      it('should return a new state signifying that one thing is loading if two things were previously loading', () => {
        const state = 2
        const nextState = loadingReducer(state, { type: actions.LOADING_DONE })

        expect(nextState).toEqual(1)
      })

    })

  })

})
