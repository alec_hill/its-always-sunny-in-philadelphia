import * as actions from '../actions'

describe('UI Actions', () => {

  describe('application init', () => {

    it('should be an available action', () => {
      expect(actions.APPLICATION_INIT).toEqual('ui/application/init')
    })

    it('should create the action', () => {
      const action = actions.applicationInit()
      expect(action.type).toEqual(actions.APPLICATION_INIT)
    })

  })

  describe('loading', () => {

    describe('loading started', () => {

      it('should be an available action', () => {
        expect(actions.LOADING_STARTED).toEqual('ui/loading/started')
      })

      it('should create the appropriate action signifying loading is true', () => {
        const action = actions.loadingStarted()
        expect(action.type).toEqual(actions.LOADING_STARTED)
      })

    })

    describe('loading done', () => {

      it('should be an available action', () => {
        expect(actions.LOADING_DONE).toEqual('ui/loading/done')
      })

      it('should create the appropriate action signifying loading is done', () => {
        const action = actions.loadingDone()
        expect(action.type).toEqual(actions.LOADING_DONE)
      })
    })

  })

})
