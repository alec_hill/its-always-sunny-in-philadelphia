import * as React from 'react'
import {FormattedMessage} from 'react-intl'

const styles = require('./LoadingIndicator.module.css')

export interface ILoadingIndicatorProps {
  isLoading: boolean
}

export class LoadingIndicator extends React.PureComponent<ILoadingIndicatorProps> {

  render() {
    return this.props.isLoading ? (
      <div className={styles.indicator}>
        <FormattedMessage id="loadingindicator.loading" defaultMessage="Loading..." />
      </div>
    ) : null
  }

}

export default LoadingIndicator
