import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import {LoadingIndicator} from '../LoadingIndicator'

const styles = require('../LoadingIndicator.module.css')

describe('LoadingIndicator component', () => {

  describe('when is loading', () => {

    it('should have an indicator', () => {
      const wrapper = enzymeIntl.shallow(<LoadingIndicator isLoading={true} />)
      expect(wrapper.find(cssClassNames(styles.indicator))).toHaveLength(1)
    })

  })

  describe('when is not loading', () => {

    it('should render nothing', () => {
      const wrapper = enzymeIntl.shallow(<LoadingIndicator isLoading={false} />)
      expect(wrapper.children()).toHaveLength(0)
    })

  })

})
