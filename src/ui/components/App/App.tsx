import * as React from 'react'
import * as classnames from 'classnames'
import {FormattedMessage} from 'react-intl'
import LoadingIndicatorContainer from '../../containers/LoadingIndicatorContainer'
import forecast from '../../../forecast'
import location from '../../../location'

// global reset styles for applicatiion
import '../../../__css__/reset.css'

const styles = require('./App.module.css')

export class App extends React.PureComponent {

  render() {
    return (
      <div className={styles.container}>
        <LoadingIndicatorContainer />
        <header className={styles.heading}>
          <h1>
            <FormattedMessage id="app.heading" defaultMessage="Its Always Sunny In..." />
          </h1>
          <location.CitySelector />
        </header>
        <forecast.ForecastContainer />
      </div>
    )
  }

}

export default App
