import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import App from '../App'
import LoadingIndicatorContainer from '../../../containers/LoadingIndicatorContainer'

const styles = require('../App.module.css')

describe('App', () => {

  describe('some general initial entry point sanity checks, to ensure tools setup correctly', () => {

    it('should render without crashing', () => {
      expect(enzymeIntl.shallow(<App />)).not.toThrowError
    })

    it('should support css modules class names being applied to components', () => {
      const wrapper = enzymeIntl.shallow(<App />)
      expect(wrapper.find(cssClassNames(styles.container))).toHaveLength(1)
    })

  })

  describe('component', () => {

    const wrapper = enzymeIntl.shallow(<App />)

    it('should have a connected loading indicator', () => {
      expect(wrapper.find(LoadingIndicatorContainer)).toHaveLength(1)
    })

  })

})
