import {
  connect,
  MapStateToProps
} from 'react-redux'
import {IAppState} from '../../domain'
import * as selectors from '../selectors'
import {
  ILoadingIndicatorProps,
  LoadingIndicator
} from '../components/LoadingIndicator/LoadingIndicator'

const mapStateToProps: MapStateToProps<ILoadingIndicatorProps, {}, IAppState> = state => ({
  isLoading: selectors.isLoading(state)
})

export default connect<ILoadingIndicatorProps>(mapStateToProps)(LoadingIndicator)
