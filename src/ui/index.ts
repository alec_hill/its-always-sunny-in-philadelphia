import * as actions from './actions'
import * as reducers from './reducers'
import * as selectors from './selectors'
import App from './components/App/App'

export default {
  actions,
  reducers,
  selectors,
  App
}
