import {SagaIterator} from 'redux-saga'
import {
  call,
  put,
  takeLatest
} from 'redux-saga/effects'
import ui from '../ui'
import forecast from '../forecast'
import render from '../renderer'

export function* watchApplicationStartSaga(): SagaIterator {
  yield takeLatest(ui.actions.APPLICATION_INIT, startApplicationSaga)
}

export function* startApplicationSaga(): SagaIterator {
  yield call(render)
  yield put(forecast.actions.fetch())
}
