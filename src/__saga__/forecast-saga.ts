import {Action} from 'redux-actions'
import {SagaIterator} from 'redux-saga'
import {
  call,
  put,
  select,
  takeLatest
} from 'redux-saga/effects'
import ui from '../ui'
import forecast from '../forecast'
import location from '../location'
import render from '../renderer'
import {IForecastApiResults} from '../domain'

export function* watchForecastSaga(): SagaIterator {
  yield takeLatest(forecast.actions.FETCH, fetchForecastSaga)
}

export function* fetchForecastSaga(): SagaIterator {
  try {
    yield put(ui.actions.loadingStarted())
    const city: string = yield select(location.selectors.city)
    const results: IForecastApiResults = yield call(forecast.service.getForecast, city)
    yield put(forecast.actions.results(results))
  } catch (err) {
    console.error('Failed to fetch forecast', err)
  } finally {
    yield put(ui.actions.loadingDone())
  }
}

export function* watchCSVSaga(): SagaIterator {
  yield takeLatest(location.actions.CSV, parseCSVSaga)
}

export function* parseCSVSaga(action: Action<string>): SagaIterator {
  try {
    const items = yield call(forecast.csvService.parse, action.payload)
    yield put(forecast.actions.results(items))
  } catch (err) {
    console.error('Could not parse CSV into forecast', err)
  }
}
