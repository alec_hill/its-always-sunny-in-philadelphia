import {
  call,
  put,
  takeLatest
} from 'redux-saga/effects'
import {
  watchApplicationStartSaga,
  startApplicationSaga
} from '../application-saga'
import ui from '../../ui'
import forecast from '../../forecast'
import render from '../../renderer'

describe('Application Saga', () => {

  describe('watcher', () => {
    const watcher = watchApplicationStartSaga()

    it('should respond to the latest application init action', () => {
      const nextEffect = watcher.next()
      expect(nextEffect.value).toEqual(takeLatest(ui.actions.APPLICATION_INIT, startApplicationSaga))
    })

  })

  describe('start application', () => {

    const iterator = startApplicationSaga()

    it('should render the application', () => {
      const nextEffect = iterator.next()
      expect(nextEffect.value).toEqual(call(render))
    })

    it('should fetch the forecast', () => {
      const nextEffect = iterator.next()
      expect(nextEffect.value).toEqual(put(forecast.actions.fetch()))
    })

  })

})
