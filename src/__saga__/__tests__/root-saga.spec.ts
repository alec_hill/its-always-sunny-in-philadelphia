import {
  all,
  put,
  fork
} from 'redux-saga/effects'
import {rootSaga} from '../root-saga'
import {watchApplicationStartSaga} from '../application-saga'
import {
  watchForecastSaga,
  watchCSVSaga
} from '../forecast-saga'
import {watchLocationChangeSaga} from '../location-saga'
import ui from '../../ui'

describe('Root Saga', () => {

  const iterator = rootSaga()

  it('should start all the sagas', () => {
    const nextEffect = iterator.next()
    expect(nextEffect.value).toEqual(all([
      watchApplicationStartSaga,
      watchForecastSaga,
      watchLocationChangeSaga,
      watchCSVSaga
    ].map(fork)))
  })

  it('should dispatch action to init application', () => {
    const nextEffect = iterator.next()
    expect(nextEffect.value).toEqual(put(ui.actions.applicationInit()))
  })

})
