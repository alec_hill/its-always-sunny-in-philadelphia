import {
  call,
  put,
  select,
  takeLatest
} from 'redux-saga/effects'
import {
  watchForecastSaga,
  watchCSVSaga,
  fetchForecastSaga,
  parseCSVSaga
} from '../forecast-saga'
import {List} from 'immutable'
import {IForecastItem} from '../../domain'
import ui from '../../ui'
import forecast from '../../forecast'
import location from '../../location'

describe('Forecast Saga', () => {

  describe('fetch forcast', () => {

    describe('watcher', () => {
      const watcher = watchForecastSaga()

      it('should respond to the latest fetch action', () => {
        const nextEffect = watcher.next()
        expect(nextEffect.value).toEqual(takeLatest(forecast.actions.FETCH, fetchForecastSaga))
      })

    })

    describe('fetch', () => {

      const iterator = fetchForecastSaga()
      const stubCity = 'nic cage'
      const stubResults = List<IForecastItem>()

      it('should dispatch loading started', () => {
        const nextEffect = iterator.next()
        expect(nextEffect.value).toEqual(put(ui.actions.loadingStarted()))
      })

      it('should select the city', () => {
        const nextEffect = iterator.next()
        expect(nextEffect.value).toEqual(select(location.selectors.city))
      })

      it('should call the forecast service', () => {
        const nextEffect = iterator.next(stubCity)
        expect(nextEffect.value).toEqual(call(forecast.service.getForecast, stubCity))
      })

      describe('on successful service response', () => {

        it('should dispatch the results', () => {
          const nextEffect = iterator.next(stubResults)
          expect(nextEffect.value).toEqual(put(forecast.actions.results(stubResults)))
        })

        it('should dispatch loading done', () => {
          const nextEffect = iterator.next()
          expect(nextEffect.value).toEqual(put(ui.actions.loadingDone()))
        })

      })

      describe('on error response service response', () => {

        const failureIterator = fetchForecastSaga()
        failureIterator.next() // iterator must bbe active before error simulated

        it('should dispatch loading done', () => {
          const err = new Response(null, {status: 400})
          const nextEffect = failureIterator.throw(err)
          expect(nextEffect.value).toEqual(put(ui.actions.loadingDone()))
        })

      })

    })

  })

  describe('CSV forcast', () => {

    describe('watcher', () => {
      const watcher = watchCSVSaga()

      it('should respond to the latest CSV action', () => {
        const nextEffect = watcher.next()
        expect(nextEffect.value).toEqual(takeLatest(location.actions.CSV, parseCSVSaga))
      })

    })

    describe('parse CSV', () => {

      const stubCSV = 'nic,cage'
      const action = {type: location.actions.CSV, payload: stubCSV}
      const iterator = parseCSVSaga(action)
      const stubResults = List<IForecastItem>()

      it('should call the CSV service', () => {
        const nextEffect = iterator.next()
        expect(nextEffect.value).toEqual(call(forecast.csvService.parse, stubCSV))
      })

      describe('on success', () => {

        it('should dispatch the results', () => {
          const nextEffect = iterator.next(stubResults)
          expect(nextEffect.value).toEqual(put(forecast.actions.results(stubResults)))
        })

      })

      describe('on error', () => {

        const failureIterator = parseCSVSaga(action)
        failureIterator.next() // iterator must bbe active before error simulated

        it('should catch error', () => {
          const err = new Error()
          const nextEffect = failureIterator.throw(err)
          expect(nextEffect.done).toBe(true)
        })

      })

    })

  })

})
