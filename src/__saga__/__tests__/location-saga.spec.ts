import {
  call,
  put,
  takeLatest
} from 'redux-saga/effects'
import {
  watchLocationChangeSaga,
  changeLocationSaga
} from '../location-saga'
import forecast from '../../forecast'
import location from '../../location'

describe('Location Saga', () => {

  describe('watcher', () => {
    const watcher = watchLocationChangeSaga()

    it('should respond to the latest location change action', () => {
      const nextEffect = watcher.next()
      expect(nextEffect.value).toEqual(takeLatest(location.actions.CHANGE_CITY, changeLocationSaga))
    })

  })

  describe('change location', () => {

    describe('when changed to a city', () => {

      const action = location.actions.changeCity('London')
      const iterator = changeLocationSaga(action)

      it('should fetch the forecast', () => {
        const nextEffect = iterator.next()
        expect(nextEffect.value).toEqual(put(forecast.actions.fetch()))
      })

    })

    describe('when changed to CSV', () => {

      const action = location.actions.changeCity('Custom CSV')
      const iterator = changeLocationSaga(action)

      it('should not do anything', () => {
        const nextEffect = iterator.next()
        expect(nextEffect.done).toBe(true)
      })

    })

  })

})
