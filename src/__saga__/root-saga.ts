import {
  SagaIterator,
  takeLatest
} from 'redux-saga'
import {
  all,
  fork,
  put
} from 'redux-saga/effects'
import ui from '../ui'
import {watchApplicationStartSaga} from './application-saga'
import {
  watchForecastSaga,
  watchCSVSaga
} from './forecast-saga'
import {watchLocationChangeSaga} from './location-saga'

export function* rootSaga(): SagaIterator {
  // start all the long running sagas
  yield all([
    watchApplicationStartSaga,
    watchForecastSaga,
    watchLocationChangeSaga,
    watchCSVSaga
  ].map(fork))
  // fire it up
  yield put(ui.actions.applicationInit())
}
