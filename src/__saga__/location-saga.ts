import {Action} from 'redux-actions'
import {SagaIterator} from 'redux-saga'
import {
  call,
  put,
  select,
  takeLatest
} from 'redux-saga/effects'
import forecast from '../forecast'
import location from '../location'

export function* watchLocationChangeSaga(): SagaIterator {
  yield takeLatest(location.actions.CHANGE_CITY, changeLocationSaga)
}

export function* changeLocationSaga(action: Action<string>): SagaIterator {
  if (action.payload !== 'Custom CSV') {
    yield put(forecast.actions.fetch())
  }
}
