import * as React from 'react'
import * as PropTypes from 'prop-types'
import createMockStore from 'redux-mock-store'
import {
  shallow,
  mount,
  render,
  ReactWrapper,
  ShallowWrapper
} from 'enzyme'
import {
  intlShape,
  IntlProvider
} from 'react-intl'
import {createAppState} from '../state'
import {IAppState} from '../domain'

// CSS modules helpers...

// turns composed ICSS mappings into css class selector
export const cssClassNames = (...icssStyle: string[]): string =>
  '.'.concat(icssStyle.map((_: string) => `${_.split(' ').join('.')}`).join('.'))

// Redux test utils....

export const storeShape = PropTypes.shape({
  subscribe: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  getState: PropTypes.func.isRequired
})

export function createStoreContext(): any {
  return { store: createMockStore<IAppState>()(createAppState()) }
}

// Intl context support for Enzyme adapted from...
// https://github.com/yahoo/react-intl/wiki/Testing-with-React-Intl#enzyme
// ignoring some complex typing for now as this uses some low level stuff thats not covered and its only for specs

/**
 * Components using the react-intl module require access to the intl context.
 * This is not available when mounting single components in Enzyme.
 * These helper functions aim to address that and wrap a valid, English-locale intl context around them.
 * Also injects store context for containers
 */
export const enzymeIntl = {

  shallow: (node: any, locale?: string, messages?: object, context: any = {}): ShallowWrapper<any, any> => {
    const intl: any = createIntlContext(locale, messages)
    const store: any = createStoreContext()
    return shallow(nodeWithIntlProp(node, intl), {
      context: Object.assign({}, context, store, {intl})
    })
  },

  mount: (node: any, locale?: string, messages?: object,
          context: any = {}, childContextTypes: any = {}): ReactWrapper<any, any>  => {
    const intl: any = createIntlContext(locale, messages)
    const store: any = createStoreContext()
    return mount(nodeWithIntlProp(node, intl), {
      context: Object.assign({}, context, store, {intl}),
      childContextTypes: Object.assign({}, { intl: intlShape, store: storeShape }, childContextTypes)
    })
  },

  render: (node: any, locale?: string, messages?: object,
           context: any = {}, childContextTypes: any = {}): Cheerio  => {
    const intl: any = createIntlContext(locale, messages)
    const store: any = createStoreContext()
    return render(nodeWithIntlProp(node, intl), {
      context: Object.assign({}, context, store, {intl}),
      childContextTypes: Object.assign({}, { intl: intlShape, store: storeShape }, childContextTypes)
    })
  }

}

// allows optional passing of locale and messages so that translation can be tested
function createIntlContext(locale?: string, messages?: object): any {
  const intlProvider: any = new IntlProvider({ locale: locale || 'en', messages: messages || {} }, {})
  const { intl } = intlProvider.getChildContext() as any
  return intl
}

// When using React-Intl `injectIntl` on components, props.intl is required.
function nodeWithIntlProp(node: any, intl: any): any {
  return React.cloneElement(node, {intl})
}
