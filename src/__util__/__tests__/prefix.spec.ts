import prefix from '../prefix'

describe('Prefix util', () => {

  const pfx = 'nic cage'
  const str = 'oscar award winning actor'
  const str2 = 'and multiple time nominee'

  describe('when prefix partial is applied with default seperator', () => {

    const name = prefix(pfx)

    it('should prefix a string joined by the default seperator', () => {
      expect(name(str)).toBe(`${pfx}/${str}`)
    })

    it('should prefix multiple strings joined by the default seperator', () => {
      const str = 'oscar winning actor'
      expect(name(str, str2)).toBe(`${pfx}/${str}/${str2}`)
    })

  })

  describe('when prefix partial is applied with custom seperator', () => {

    const name = prefix(pfx, '-')

    it('should prefix a string joined by the default seperator', () => {
      expect(name(str)).toBe(`${pfx}-${str}`)
    })

    it('should prefix multiple strings joined by the default seperator', () => {
      const str = 'oscar winning actor'
      expect(name(str, str2)).toBe(`${pfx}-${str}-${str2}`)
    })

  })

})
