import * as React from 'react'
import {render} from 'react-dom'
import store from './store'
import {Provider as StoreProvider} from 'react-redux'
import {
  IntlProvider,
  addLocaleData
} from 'react-intl'
import * as en from 'react-intl/locale-data/en'
import ui from './ui'

addLocaleData(en)

export const DEFAULT_LOCALE = 'en'
export const RENDER_TO_ID = 'root'

export default () => render(
  <IntlProvider locale={DEFAULT_LOCALE} messages={{}} defaultLocale={DEFAULT_LOCALE}>
    <StoreProvider store={store}>
      <ui.App />
    </StoreProvider>
  </IntlProvider>,
  document.getElementById(RENDER_TO_ID)
)
