import {IAppState} from './domain'

export const createAppState = (): IAppState => ({
  ui: {
    loading: 0
  },
  location: {
    city: 'London'
  },
  forecast: {
    results: null
  }
})

export const INITIAL_STATE = createAppState()
