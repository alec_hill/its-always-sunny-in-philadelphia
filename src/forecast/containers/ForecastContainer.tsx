import {
  connect,
  MapStateToProps
} from 'react-redux'
import {IAppState} from '../../domain'
import * as selectors from '../selectors'
import {
  IForecastProps,
  Forecast
} from '../components/Forecast/Forecast'

const mapStateToProps: MapStateToProps<IForecastProps, {}, IAppState> = state => ({
  items: selectors.results(state)
})

export default connect<IForecastProps>(mapStateToProps)(Forecast)
