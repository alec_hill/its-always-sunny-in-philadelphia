import {List} from 'immutable'
import * as url from 'url'
import {
  IForecastItem,
  IForecastApiResults
} from '../domain'

export const URL
  = 'https://api.openweathermap.org/data/2.5/forecast?units=metric&appid=289b33fc1f37639eac2f57154ab2499c'

type FetchShape = typeof fetch

export const getForecast = (city: string, fetch: FetchShape = window.fetch): Promise<IForecastApiResults> => {
  const apiUrl = url.parse(URL, true)
  delete apiUrl.search
  apiUrl.query = Object.assign({}, apiUrl.query, {q: `${city},uk`})
  return fetch(url.format(apiUrl)).then((response: Response) => {
    if (response.status >= 200 && response.status < 300) {
      return response.json().then((json: any) => Promise.resolve(
        List<IForecastItem>(json.list.map((item: any): IForecastItem => ({
          date: item.dt_txt,
          temp: item.main.temp,
          icon: item.weather.length && item.weather[0].icon
        }))
      )))
    } else {
      throw response
    }
  })
}

export const iconUrl = (icon: string) => `https://openweathermap.org/img/w/${icon}.png`

/*

{
	"cod": "200",
	"message": 0.0033,
	"cnt": 40,
	"list": [{
		"dt": 1520100000,
		"main": {
			"temp": 275.3,
			"temp_min": 275.3,
			"temp_max": 276.061,
			"pressure": 998.15,
			"sea_level": 1005.86,
			"grnd_level": 998.15,
			"humidity": 97,
			"temp_kf": -0.76
		},
		"weather": [{
			"id": 500,
			"main": "Rain",
			"description": "light rain",
			"icon": "10n"
		}],
		"clouds": {
			"all": 88
		},
		"wind": {
			"speed": 5.26,
			"deg": 96.0045
		},
		"rain": {
			"3h": 0.0325
		},
		"snow": {},
		"sys": {
			"pod": "n"
		},
		"dt_txt": "2018-03-03 18:00:00"
	}],
	"city": {
		"id": 2643743,
		"name": "London",
		"coord": {
			"lat": 51.5085,
			"lon": -0.1258
		},
		"country": "GB",
		"population": 1000000
	}
}

*/
