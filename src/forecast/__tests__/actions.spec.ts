import * as actions from '../actions'
import {List} from 'immutable'
import {
  IForecastApiResults,
  IForecastItem
} from '../../domain'

describe('Forecast Actions', () => {

  describe('fetch', () => {

    it('should be an available action', () => {
      expect(actions.FETCH).toEqual('forecast/fetch')
    })

    it('should create the appropriate action signifying loading is true', () => {
      const action = actions.fetch()

      expect(action.type).toEqual(actions.FETCH)
    })

  })

  describe('results', () => {

    it('should be an available action', () => {
      expect(actions.RESULTS).toEqual('forecast/results')
    })

    it('should create an action with correct name and results list as payload', () => {
      const stubResults = List<IForecastItem>()
      const action = actions.results(stubResults)

      expect(action.type).toEqual(actions.RESULTS)
      expect(action.payload).toEqual(stubResults)
    })

  })

})
