import * as React from 'react'
import {List} from 'immutable'
import {IForecastItem} from '../../domain'
import * as service from '../service'

describe('service', () => {

  // tslint:disable
  const stubJSON = JSON.stringify({
    "cod": "200",
    "message": 0.0033,
    "cnt": 40,
    "list": [{
      "dt": 1520100000,
      "main": {
        "temp": 275.3,
        "temp_min": 275.3,
        "temp_max": 276.061,
        "pressure": 998.15,
        "sea_level": 1005.86,
        "grnd_level": 998.15,
        "humidity": 97,
        "temp_kf": -0.76
      },
      "weather": [{
        "id": 500,
        "main": "Rain",
        "description": "light rain",
        "icon": "10n"
      }],
      "clouds": {
        "all": 88
      },
      "wind": {
        "speed": 5.26,
        "deg": 96.0045
      },
      "rain": {
        "3h": 0.0325
      },
      "snow": {},
      "sys": {
        "pod": "n"
      },
      "dt_txt": "2018-03-03 18:00:00"
    }],
    "city": {
      "id": 2643743,
      "name": "London",
      "coord": {
        "lat": 51.5085,
        "lon": -0.1258
      },
      "country": "GB",
      "population": 1000000
    }
  })
  // tslint:enable

  describe('iconUrl', () => {

    it('should turn an icon code into an icon url', () => {
      const icon = 'nic cage'
      expect(service.iconUrl(icon)).toBe(`https://openweathermap.org/img/w/${icon}.png`)
    })

  })

  describe('getForecast', () => {

    const city = 'nic cage'
    const mockFetch = jest.fn()

    beforeEach(mockFetch.mockReset)

    describe('when request is successful', () => {

      it('should parse JSON reults', () => {
        mockFetch.mockReturnValueOnce(Promise.resolve(new Response(stubJSON, {status: 200})))

        const csv = '2016-06-01 00:00:00,66.6,nic\n2016-06-01 03:00:00,99.9,cage'
        return service.getForecast(city, mockFetch).then(results => {
          expect(results).toEqual(List<IForecastItem>([
            {date: '2018-03-03 18:00:00', temp: 275.3, icon: '10n'}
          ]))
        })
      })

    })

    describe('when request is not successful', () => {

      it('should reject with the response', () => {
        const response = new Response('', {status: 404})
        mockFetch.mockReturnValueOnce(Promise.resolve(response))

        const csv = '2016-06-01 00:00:00,66.6,nic\n2016-06-01 03:00:00,99.9,cage'
        return service.getForecast(city, mockFetch).catch(res => {
          expect(res).toBe(response)
        })
      })

    })

  })

})
