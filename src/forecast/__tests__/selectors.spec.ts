import {createAppState} from '../../state'
import * as selectors from '../selectors'
import {
  List,
  Map
} from 'immutable'
import {
  IForecastItems,
  IForecastItem
} from '../../domain'

describe('Forecast selectors', () => {

  describe('results', () => {

    it('should get the forecast from the state', () => {
      const results = Map<string, IForecastItems>({
        '2016-06-01': List<IForecastItem>([
          {date: '2016-06-01 00:00:00', temp: 66.6, icon: 'nic'},
          {date: '2016-06-01 03:00:00', temp: 99.9, icon: 'cage'}
        ]),
        '2016-06-02': List<IForecastItem>([
          {date: '2016-06-02 00:00:00', temp: 11.1, icon: 'nicolas'}
        ])
      })
      const state = Object.assign({}, createAppState(), {forecast: {results}})
      expect(selectors.results(state)).toEqual(results)
    })

  })

})
