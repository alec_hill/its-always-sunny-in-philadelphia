import {
  List,
  Map
} from 'immutable'
import {
  IForecastItems,
  IForecastItem,
  IForecastApiResults
} from '../../domain'
import * as actions from '../actions'
import * as reducers from '../reducers'
import * as selectors from '../selectors'

describe('Forecast Reducers', () => {

  describe('results reducer', () => {

    const apiResults: IForecastApiResults = List<IForecastItem>([
      {date: '2016-06-01 00:00:00', temp: 66.6, icon: 'nic'},
      {date: '2016-06-01 03:00:00', temp: 99.9, icon: 'cage'},
      {date: '2016-06-02 00:00:00', temp: 11.1, icon: 'nicolas'}
    ])

    it('should defaut if previous state is not specified', () => {
      const action = { type: 'nic cage' }
      expect(reducers.results(undefined, action)).toBe(null)
    })

    it('should return the current state if action is given but not matched', () => {
      const state = Map<string, List<IForecastItem>>()
      expect(reducers.results(state, { type: 'nic cage' })).toEqual(state)
    })

    it('should group forecast items by date into state', () => {
      const state = Map<string, List<IForecastItem>>()
      const action = {type: actions.RESULTS, payload: apiResults}
      const nextState = reducers.results(state, action)
      expect(nextState).toEqual(Map<string, IForecastItems>({
        '2016-06-01': List<IForecastItem>([
          {date: '2016-06-01 00:00:00', temp: 66.6, icon: 'nic'},
          {date: '2016-06-01 03:00:00', temp: 99.9, icon: 'cage'}
        ]),
        '2016-06-02': List<IForecastItem>([
          {date: '2016-06-02 00:00:00', temp: 11.1, icon: 'nicolas'}
        ])
      }))
    })

  })

})
