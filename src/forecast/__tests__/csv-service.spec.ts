import * as React from 'react'
import {List} from 'immutable'
import {IForecastItem} from '../../domain'
import * as service from '../csv-service'

describe('CSV service', () => {

  describe('parse', () => {

    it('should parse CSV with icons', () => {
      const csv = '2016-06-01 00:00:00,66.6,nic\n2016-06-01 03:00:00,99.9,cage'
      return service.parse(csv).then(results => {
        expect(results).toEqual(List<IForecastItem>([
          {date: '2016-06-01 00:00:00', temp: 66.6, icon: 'nic'},
          {date: '2016-06-01 03:00:00', temp: 99.9, icon: 'cage'}
        ]))
      })
    })

    it('should parse CSV without icons', () => {
      const csv = '2016-06-01 00:00:00,66.6\n2016-06-01 03:00:00,99.9'
      return service.parse(csv).then(results => {
        expect(results).toEqual(List<IForecastItem>([
          {date: '2016-06-01 00:00:00', temp: 66.6},
          {date: '2016-06-01 03:00:00', temp: 99.9}
        ]))
      })
    })

    it('should parse ignoring extraneous whitespace', () => {
      const csv = '2016-06-01 00:00:00,   66.6  \n 2016-06-01 03:00:00,   99.9'
      return service.parse(csv).then(results => {
        expect(results).toEqual(List<IForecastItem>([
          {date: '2016-06-01 00:00:00', temp: 66.6},
          {date: '2016-06-01 03:00:00', temp: 99.9}
        ]))
      })
    })

  })

})
