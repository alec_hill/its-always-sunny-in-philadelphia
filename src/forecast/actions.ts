import {createAction} from 'redux-actions'
import {IForecastApiResults} from '../domain'
import prefix from '../__util__/prefix'

const name = prefix('forecast')

export const FETCH = name('fetch')
export const RESULTS = name('results')

export const fetch = createAction(FETCH)
export const results = createAction<IForecastApiResults>(RESULTS)
