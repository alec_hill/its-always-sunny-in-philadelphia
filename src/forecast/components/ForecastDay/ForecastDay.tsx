import * as React from 'react'
import * as classnames from 'classnames'
import {FormattedDate} from 'react-intl'
import {List} from 'immutable'
import {IForecastItem} from '../../../domain'
import {
  IForecastPeriodProps,
  ForecastPeriod
} from '../ForecastPeriod/ForecastPeriod'

const styles = require('./ForecastDay.module.css')

export interface IForecastDayProps {
  date: string
  periods: List<IForecastItem>
}

export class ForecastDay extends React.PureComponent<IForecastDayProps> {

  render() {
    const {date, periods} = this.props
    return (
      <div className={styles.day}>
        <div className={styles.date}><FormattedDate value={date} /></div>
        <div className={styles.periods}>
          {periods.map((item, i) => <ForecastPeriod key={i} {...item} />)}
        </div>
      </div>
    )
  }

}

export default ForecastDay
