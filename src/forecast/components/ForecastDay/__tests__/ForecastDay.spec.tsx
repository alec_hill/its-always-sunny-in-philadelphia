import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import {List} from 'immutable'
import {IForecastItem} from '../../../../domain'
import {ForecastDay} from '../ForecastDay'
import {ForecastPeriod} from '../../ForecastPeriod/ForecastPeriod'

const styles = require('../ForecastDay.module.css')

describe('ForcastDay component', () => {

  const date = '2016-06-01'

  const item1: IForecastItem = {
    date: '2016-06-01 00:00:00',
    temp: 66.6
  }

  const item2: IForecastItem = {
    date: '2016-06-01 03:00:00',
    temp: 99.9
  }

  const periods = List<IForecastItem>([item1, item2])

  it('should contain the formatted date as a header', () => {
    const wrapper = enzymeIntl.shallow(<ForecastDay periods={periods} date={date} />)
    expect(wrapper.find('FormattedDate').props().value).toBe(date)
  })

  it('should contain a ForcastPeriod for each item in the', () => {
    const wrapper = enzymeIntl.shallow(<ForecastDay periods={periods} date={date} />)
    expect(wrapper.find(ForecastPeriod)).toHaveLength(2)
  })

})
