import * as React from 'react'
import {IForecastItems} from '../../../domain'
import {ForecastDay} from '../ForecastDay/ForecastDay'

const styles = require('./Forecast.module.css')

export interface IForecastProps {
  items?: IForecastItems
}

export class Forecast extends React.PureComponent<IForecastProps> {

  render() {
    const {items} = this.props
    return items ? (
      <div className={styles.forecast}>
        {items.entrySeq().map(([date, periods]) => <ForecastDay key={date} date={date} periods={periods} />)}
      </div>
    ) : null
  }

}

export default Forecast
