import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import {
  Map,
  List
} from 'immutable'
import {IForecastItem} from '../../../../domain'
import {Forecast} from '../Forecast'
import {ForecastDay} from '../../ForecastDay/ForecastDay'

const styles = require('../Forecast.module.css')

describe('Forecast component', () => {

  const item1: IForecastItem = {
    date: '2016-06-01 00:00:00',
    temp: 66.6
  }

  const item2: IForecastItem = {
    date: '2017-06-01 03:00:00',
    temp: 99.9
  }

  const results = Map<string, List<IForecastItem>>({
    '2016-06-01': List<IForecastItem>([item1]),
    '2017-06-01': List<IForecastItem>([item2])
  })

  describe('when there are results', () => {

    it('should contain a ForecastDay for each day in the results', () => {
      const wrapper = enzymeIntl.shallow(<Forecast items={results} />)
      expect(wrapper.find(ForecastDay)).toHaveLength(2)
    })

  })

  describe('when there are no results', () => {

    it('should not render anything', () => {
      const wrapper = enzymeIntl.shallow(<Forecast />)
      expect(wrapper.children()).toHaveLength(0)
    })

  })

})
