import * as React from 'react'
import {
  FormattedDate,
  FormattedTime,
  FormattedNumber
} from 'react-intl'
import {iconUrl} from '../../service'

const styles = require('./ForecastPeriod.module.css')

export interface IForecastPeriodProps {
  date: string
  temp: number
  icon?: string
}

export class ForecastPeriod extends React.PureComponent<IForecastPeriodProps> {

  render() {
    const {date, temp, icon} = this.props
    return (
      <div className={styles.period}>
        <FormattedTime value={date} /><br />
        <FormattedNumber value={temp} />&#8451;<br />
        {!!icon && <img src={iconUrl(icon)} />}
      </div>
    )
  }

}

export default ForecastPeriod
