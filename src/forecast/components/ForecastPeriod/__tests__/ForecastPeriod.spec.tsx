import * as React from 'react'
import {
  enzymeIntl,
  cssClassNames
} from '../../../../__tests__/test-utils'
import {IForecastItem} from '../../../../domain'
import {ForecastPeriod} from '../ForecastPeriod'
import {iconUrl} from '../../../service'

const styles = require('../ForecastPeriod.module.css')

describe('ForecastPeriod component', () => {

  const item: IForecastItem = {
    date: '2016-06-01 00:00:00',
    temp: 66.6
  }

  const itemWithIcon: IForecastItem = {icon: 'nic cage', ...item}

  it('should contain the formatted time', () => {
    const wrapper = enzymeIntl.shallow(<ForecastPeriod {...itemWithIcon} />)
    expect(wrapper.find('FormattedTime').props().value).toBe(item.date)
  })

  it('should contain the formatted temperature', () => {
    const wrapper = enzymeIntl.shallow(<ForecastPeriod {...itemWithIcon} />)
    expect(wrapper.find('FormattedNumber').props().value).toBe(item.temp)
  })

  describe('when has no icon', () => {

    it('should not contain an icon image', () => {
      const wrapper = enzymeIntl.shallow(<ForecastPeriod {...item} />)
      expect(wrapper.find('img')).toHaveLength(0)
    })

  })

  describe('when has an icon', () => {

    it('should contain an image with api icon src', () => {
      const wrapper = enzymeIntl.shallow(<ForecastPeriod {...itemWithIcon} />)
      expect(wrapper.find('img').props().src).toBe(iconUrl(itemWithIcon.icon))
    })

  })

})
