import {
  Selector,
  createSelector
} from 'reselect'
import {List} from 'immutable'
import {
  IAppState,
  IForecastItems
} from '../domain'

export const results: Selector<IAppState, IForecastItems> = state =>  state.forecast.results
