import {List} from 'immutable'
import {
  IForecastItem,
  IForecastApiResults
} from '../domain'

export const parse = (csv: string): Promise<IForecastApiResults> => Promise.resolve(
    List<string>(csv.trim().split('\n')).map<List<string>>(line =>
      List<string>(line.trim().split(',').map(part => part.trim()))
    ).map<IForecastItem>(parts => ({
      date: parts.get(0),
      temp: parts.get(1) && parseFloat(parts.get(1)),
      icon: parts.get(2) && parts.get(2)
    })
  ).toList()
)
