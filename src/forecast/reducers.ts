import {handleActions} from 'redux-actions'
import * as actions from './actions'
import * as selectors from './selectors'
import { INITIAL_STATE } from '../state'
import {
  IForecastItems,
  IForecastApiResults
} from '../domain'

export const results = handleActions<IForecastItems, IForecastApiResults>({

  [actions.RESULTS]: (state, action) => action.payload.groupBy<string>(item => item.date.split(' ')[0])
                                                        .map(group => group.toList())
                                                          .toMap()

}, selectors.results(INITIAL_STATE))
