import {
  combineReducers,
  Reducer
} from 'redux'
import {
  IAppState,
  IUserInterface,
  IForecast,
  ILocation
} from './domain'
import {emptyReducer} from './__util__/reducer-utils'
import {INITIAL_STATE} from './state'
import ui from './ui'
import forecast from './forecast'
import location from './location'

export default combineReducers<IAppState>({
  ui: combineReducers<IUserInterface>({
    loading: ui.reducers.loading
  }),
  location: combineReducers<ILocation>({
    city: location.reducers.city
  }),
  forecast: combineReducers<IForecast>({
    results: forecast.reducers.results
  })
})
