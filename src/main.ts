import render from './renderer'
import {rootSaga} from './__saga__/root-saga'
import sagaExecutor from './__saga__/saga-executor'

// start application up...
sagaExecutor.run(rootSaga)
