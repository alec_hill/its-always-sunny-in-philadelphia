import {
  createStore,
  combineReducers,
  applyMiddleware,
  Middleware,
  compose,
  Store
} from 'redux'
import rootReducer from './root-reducer'
import {Action} from 'redux-actions'
import {createLogger} from 'redux-logger'
import sagaExecutor from './__saga__/saga-executor'
import {INITIAL_STATE} from './state'
import {IAppState} from './domain'

const middlewares: Middleware[] = [sagaExecutor, createLogger()]

const middleware = applyMiddleware(...middlewares)

export default createStore<IAppState>(
  rootReducer,
  INITIAL_STATE,
  middleware
)
