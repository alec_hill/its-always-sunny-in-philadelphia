import {
  List,
  Map
} from 'immutable'

export interface IAppState {
  ui: IUserInterface
  location: ILocation
  forecast: IForecast
}

export interface IUserInterface {
  loading: number
}

export interface ILocation {
  city: string
}

export interface IForecast {
  results: IForecastItems | null
}

export interface IForecastItem {
  date: string
  temp: number
  icon?: string
}

export type IForecastApiResults = List<IForecastItem>

export type IForecastItems = Map<string, List<IForecastItem>>
